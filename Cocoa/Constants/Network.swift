import Foundation
import CoreLocation
import Network
import SystemConfiguration

class Network: Echoable
{
    enum Status {
        /// The network connection has not been checked for yet
        case cold
        /// The network could not establish a connection
        case notReachable
        /// The network is reachable only via Cellular
        case reachableOnlyViaCellular
        /// The network may or may not be reachable via Cellular, but is definitely reachable via WiFi
        case reachablePossiblyViaCellularAndViaWiFi
        
        /// The current status of the network. Default is set to .cold
        static var current = Status.cold
        
        /// Whether or not the network has a current internet connection
        var connected: Bool {
            return Status.current == .reachableOnlyViaCellular ||
                   Status.current == .reachablePossiblyViaCellularAndViaWiFi
        }

        @available(iOS 12.0, *)
        init(path: NWPath) {
            guard path.status == .satisfied else {
                self = .notReachable
                return
            }
            
            if path.isExpensive {
                self = .reachableOnlyViaCellular
                return
            }
            
            self = .reachablePossiblyViaCellularAndViaWiFi
        }
    }

    private var networkStatusTimer: Timer?

    init()
    {
        self.networkStatusTimer = Timer.scheduledTimer(timeInterval: 1.0,
                                                       target: self,
                                                       selector: #selector(self.updateNetworkStatus(sender:)),
                                                       userInfo: nil,
                                                       repeats: true)
        
        self.startObserving()
    }
    
    // MARK: -Echo Handler
    func observe(object: EchoObject, dispatcher: Dispatchable, toObservers: [String])
    {
        self.observeFor(object: object, dispatcher: dispatcher, toObservers: toObservers, target: NetworkObject.Target.networkIntervalUpdate) { value in
            let newInterval = value as? TimeInterval ?? 1.0
            
            self.networkStatusTimer = Timer.scheduledTimer(timeInterval: newInterval,
                                                           target: self,
                                                           selector: #selector(self.updateNetworkStatus(sender:)),
                                                           userInfo: nil,
                                                           repeats: true)
        }
    }
    
    // MARK: -Network Status Updates
    @objc func updateNetworkStatus(sender: Timer)
    {
        if #available(iOS 12.0, *) {
            let monitor = NWPathMonitor()
            
            monitor.pathUpdateHandler = { path in
                let status = Status(path: path)

                switch status {
                    case .cold:
                        // We havent checked for a connection yet
                        let object: NetworkObject = .networkStatus(status)
                        object.emit(qos: .main, dispatcher: self, toObservers: nil)
                    
                    case .notReachable, .reachableOnlyViaCellular, .reachablePossiblyViaCellularAndViaWiFi:
                        guard Network.Status.current != status else { return }
                        
                        let object: NetworkObject = .networkStatus(status)
                        object.emit(qos: .main, dispatcher: self, toObservers: nil)
                }
                
                Network.Status.current = status
            }
            
            monitor.start(queue: .main)
        }
    }
    
    deinit
    {
        self.networkStatusTimer?.invalidate()
        self.stopObserving()
    }
}
