import Foundation
import UserNotifications

class Time: Echoable
{
    struct DateTime {
        let month: Int
        let day: Int
        let year: Int
        let hour: Int
        let minute: Int
        let second: Int
        let timeZone: TimeZone?
        
        func asComponents() -> DateComponents {
            var components = DateComponents()
            
            components.month = self.month
            components.day = self.day
            components.year = self.year
            components.hour = self.hour
            components.minute = self.minute
            components.second = self.second
            components.timeZone = self.timeZone
            
            return components
        }
        
        func asDate() -> Date? {
            return Calendar.current.date(from: self.asComponents())
        }
        
        init(month: Int, day: Int, year: Int, hour: Int, minute: Int, second: Int, timeZone: TimeZone? = nil)
        {
            self.month = month
            self.day = day
            self.year = year
            self.hour = hour
            self.minute = minute
            self.second = second
            self.timeZone = timeZone
        }
    }
    
    private var timeTimer: Timer?
    
    init()
    {
        self.timeTimer = Timer.scheduledTimer(timeInterval: 1.0,
                                              target: self,
                                              selector: #selector(self.updateTime(sender:)),
                                              userInfo: nil,
                                              repeats: true)
        
        self.startObserving()
    }
    
    // MARK: -Echo Handler
    func observe(object: EchoObject, dispatcher: Dispatchable, toObservers: [String])
    {
        self.observeFor(object: object,
                        dispatcher: dispatcher,
                        toObservers: toObservers,
                        target: TimeObject.Target.timeIntervalUpdated) { value in
                            
            let newInterval = value as? TimeInterval ?? 1.0
            
            self.timeTimer = Timer.scheduledTimer(timeInterval: newInterval,
                                                  target: self,
                                                  selector: #selector(self.updateTime(sender:)),
                                                  userInfo: nil,
                                                  repeats: true)
        }
    }
    
    // MARK: -Time Updates
    @objc func updateTime(sender: Timer)
    {
        let date = Date()
        let timeZone = TimeZone.current
        
        let month = Calendar.current.component(.month, from: date)
        let day = Calendar.current.component(.day, from: date)
        let year = Calendar.current.component(.year, from: date)
        let hour = Calendar.current.component(.hour, from: date)
        let minute = Calendar.current.component(.minute, from: date)
        let second = Calendar.current.component(.second, from: date)
        
        let dateTime = DateTime(month: month, day: day, year: year, hour: hour, minute: minute, second: second, timeZone: timeZone)
        
        let object: TimeObject = .timePassed(dateTime)
        object.emit(qos: .main, dispatcher: self)
    }
    
    // MARK: -Notification Scheduling
    static func scheduleLocalNotification(identifier: String,
                                          title: String,
                                          body: String,
                                          dateTime: DateTime,
                                          repeats: Bool=false,
                                          userInfo: [String: Any] = [:],
                                          completion: ((Error?) -> Void)? = nil)
    {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default
        content.userInfo = userInfo

        let trigger = UNCalendarNotificationTrigger(dateMatching: dateTime.asComponents(), repeats: repeats)
        
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { error in
            completion?(error)
        }
    }
    
    deinit
    {
        self.timeTimer?.invalidate()
        self.stopObserving()
    }
}
