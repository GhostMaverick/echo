//
//  Echo.swift
//  Echo
//
//  Created by Kai Lamarr on 11/5/17.
//  Copyright © 2017 Kai Lamarr. All rights reserved.
//  __version__ 2.0 (12/15/2019)

import UIKit

/// Echo is a lightweight framework to send small pieces of data
/// across an application, and a way of triggering of events upon receiving
/// that data. It is built on top of iOS' Notification Center and GCD,
/// but has similitaries to the React paradigm. This removes the need for
/// unnecessary protocols/delegates or callbacks, although implementing these
/// alongside Echo is supported as well.
class Echo
{
    /// Echo Quality of Service for a dispatched Echo object.
    enum QOS: String {
        /// background: Useful for behind-the-scenes work, such as API calls
        case background = "EchoBackgroundDispatchQueue"
        
        /// main: Useful for UI updates
        case main = "EchoMainDispatchQueue"
    }
    
    /// Echo Logging Types
    enum Log: String {
        /// Used for logging (unimportant) basic information
        case info
        
        /// Used for logging basic information
        case debug
        
        /// Used for logging warnings
        case warning
        
        /// Used for logging an error
        case error
    }
    
    /// Echo Log Console Types
    enum LogConsole {
        /// Used to log to the Echo console
        case echo
        
        /// Used to log to the SwiftyBeaver console
        case swiftyBeaver
    }
    
    /// Echo's current observers. Access any observer is accessed by using
    /// the observers Echo name, i.e., observers[Network.name]
    private(set) static var observers = [String: Observable]()
    
    /// The current Console
    static var logConsole: LogConsole = .echo
    
    /// Enable Logging. If false, skips writing to any Console,
    static var isLoggingEnabled: Bool = true
    
    /// Returns the Echo log path (defaults to the Documents dir)
    static var logPath: URL {
        let dest = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return dest.appendingPathComponent("echo.txt")
    }
    
    
    /// Echo Constant Initializer. Usually called in AppDelegate.
    /// - parameter constants: Static continously updating echoables, i.e, Network
    static func initalize(_ constants: [Echoable])
    {
        for constant in constants {
            Echo.observers[constant.name] = constant
        }
    }
    
    /// Echo Console Logging. Can also be fused with [SwiftyBeaver](https://github.com/SwiftyBeaver/SwiftyBeaver) for even easier log reading
    /// - parameter prefix: An arbitrary log prefix
    /// - parameter message: The log message
    /// - parameter type: The type of log
    static func log(prefix: String = "[Echo]", message: Any? = nil, console: LogConsole = .echo,
                    type: Log = .debug, file: String = #file, function: String = #function,
                    line: Int = #line, column: Int = #column)
    {
        let message = prefix + " \(Echo.logPath.lastPathComponent)():\(line) " + "\(message ?? "")\n"
        Echo.write(message: message, type: type, console: console)
    }
    
    /// Echo Console Logging. Can also be fused with [SwiftyBeaver](https://github.com/SwiftyBeaver/SwiftyBeaver) for even easier log reading
    /// - parameter prefix: An arbitrary log prefix
    /// - parameter object: The emitted Echo Object
    /// - parameter dispatcher: The Echo dispatcher that dispatched the Echo object
    /// - parameter observer: The Echo Observer that observed the dispatched Echo Object
    /// - parameter message: The log message
    /// - parameter type: The type of log
    static func log(prefix: String = "[Echo]", object: EchoObject, dispatcher: Dispatchable, observer: Observable,
                    message: Any? = nil, console: LogConsole = .echo, type: Log = .debug, file: String = #file,
                    function: String = #function, line: Int = #line, column: Int = #column)
    {
        let message = prefix + " \(Echo.logPath.lastPathComponent)():\(line) " + "\(observer.name) observed an Object: \(object) dispatched from \(dispatcher.name). \(message ?? "")\n"
        Echo.write(message: message, type: type, console: console)
    }
    
    // Write to the Echo Log Console
    static func write(message: Any?, type: Log, console: LogConsole)
    {
        guard Echo.isLoggingEnabled else { return }

        var logMessage = "\(message ?? "")"
        
        switch type {
            case .info: logMessage = "💙 "+logMessage
            case .debug: logMessage = "💚 "+logMessage
            case .warning: logMessage = "💛 "+logMessage
            case .error: logMessage = "❤️ "+logMessage
        }

        switch Echo.logConsole {
            case .echo:
                try? logMessage.write(to: Echo.logPath, atomically: false, encoding: .utf8)
                
            case .swiftyBeaver:
                break
        }
    }
    
    /// Read from the Echo Log Console
    static func read(completion: ((String?) -> Void))
    {
        switch Echo.logConsole {
            case .echo:
                let logMessage = try? String(contentsOf: Echo.logPath, encoding: .utf8)
                completion(logMessage)
            
            case .swiftyBeaver:
                break
        }
    }
    
    /// Add an Echo Observer
    /// - parameter observer: The Echo Observer to be added to Echo
    static func addObserver(_ observer: Observable)
    {
        var didAddObserver = false
        
        if Echo.observers[observer.name] == nil {
            Echo.observers[observer.name] = observer
            didAddObserver = true
        }
        
        if didAddObserver {
            NotificationCenter.default.addObserver(forName: Notification.Name.EchoObjectEmittedNotification,
                                                   object: nil, queue: nil) { notification in
                DispatchQueue.main.async {
                    guard let userInfo = notification.userInfo,
                          let _ = userInfo["notificationName"] as? Notification.Name,
                          let toObservers = userInfo["toObservers"] as? [String],
                          let dispatcher = userInfo["dispatcher"] as? Dispatchable,
                          let echoObject = userInfo["echoObject"] as? EchoObject else {
                            return
                    }
                    
                    if Echo.observers[observer.name] != nil {
                        Echo.log(object: echoObject, dispatcher: dispatcher, observer: observer)
                        
                        observer.observe(object: echoObject,
                                         dispatcher: dispatcher,
                                         toObservers: toObservers)
                    }
                }
            }
            
            Echo.log(message: "\(observer.name) started observing.")
        }
    }
    
    /// Remove an Echo Observer
    /// - parameter observer: The Echo Observer to be removed from Echo
    static func removeObserver(_ observer: Observable)
    {
        if Echo.observers[observer.name] != nil {
            Echo.observers[observer.name] = nil
            
            NotificationCenter.default.removeObserver(observer, name: Notification.Name.EchoObjectEmittedNotification, object: nil)
            
            Echo.log(message: "\(observer.name) stopped observing.")
        }
    }
    
    /// Remove all Echo Observers
    /// - parameter observers: The Echo Observers to be removed from Echo
    static func removeObservers(_ observers: [Observable])
    {
        for observer in observers {
            Echo.removeObserver(observer)
        }
    }
}


/// The Echo Object Protocol that all Echo Observers *must* conform to
protocol EchoObject
{
    /// name: The concrete name of the emitted Echo object
    var name: String { get }
    
    /// value: The value of the emitted Echo object
    var value: (Any) { get }
    
    /// emit: The function to be called when you're ready to fire an Echo Object
    /// - parameter after: An arbitrary delay before firing the Echo Object
    /// - parameter qos: The Quality of Service of the Echo Object
    /// - parameter dispatcher: The Echo Object dispatcher
    /// - parameter toObservers: The intended Observer Echo names for the Echo Object. Default is all (nil)
    func emit(after: TimeInterval, qos: Echo.QOS, dispatcher: Dispatchable, toObservers: [String]?)
}

/// The Echo Object Protocol extension defaut implementation
extension EchoObject
{
    func emit(after: TimeInterval = 0.0,
              qos: Echo.QOS,
              dispatcher: Dispatchable,
              toObservers: [String]? = nil)
    {
        let notificationName = Notification.Name.EchoObjectEmittedNotification
        let dispatchQOS: DispatchQoS = qos == .background ? .background : .userInteractive
        let dispatchQueue = DispatchQueue(label: qos.rawValue, qos: dispatchQOS)
        
        let userInfo: [AnyHashable: Any] = [
            "notificationName": notificationName,
            "echoObject": self,
            "dispatcher": dispatcher,
            "toObservers": toObservers ?? Echo.observers.map { $0.key },
        ]

        dispatchQueue.asyncAfter(deadline: .now() + after) {
            NotificationCenter.default.post(name: notificationName,
                                            object: nil,
                                            userInfo: userInfo)
            
            Echo.log(message: "\(dispatcher.name) emitted an Object \(self), intended for \(toObservers ?? ["all observers"]) after \(after)s.")
        }
    }
}

/// Echo Notification Names
extension Notification.Name
{
    /// Fired automatically when -emit is called on an Echo Object (isTestObject will be false)
    static let EchoObjectEmittedNotification = Notification.Name("EchoObjectEmittedNotification")
}

