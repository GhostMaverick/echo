//
//  Echoable.swift
//
//  Created by Kai Lamarr on 11/5/17.
//  Copyright © 2017 Kai Lamarr. All rights reserved.
//  __version__ 2.0 (12/15/2019)

import UIKit

/// Echo's Echoable class Protocol. This is essentially a fusion of both
/// the Observable and Dispatchable Protocols
protocol Echoable: Observable, Dispatchable { }

/// The Echoable Protocol default extension implementation
extension Echoable
{
    /// name (class): The name of the Observable class, usually just the name of the class itself
    static var name: String {
        get {
            let className = String(describing: type(of: self))
            return className.components(separatedBy: ".").first ?? ""
        }
        set { self.name = newValue }
    }
    
    ///name (instance): The name of the Observable class, usually just the name of the class itself
    var name: String {
        get { return String(describing: type(of: self)) }
        set { self.name = newValue }
    }
}

/// The Echo Echoable base UIViewController class. Any UIViewController
/// that inherits from this will be able to emit an Echo Object, as well
/// as observe Echo Objects
class EchoableViewController: UIViewController, Echoable
{
    /// The default Echo Echoable handler. If you do not override this in a subclassed view controller,
    /// nothing will happen
    func observe(object: EchoObject,
                 dispatcher: Dispatchable,
                 toObservers: [String])
    {
        // Handled by subclasses of EchoViewController
    }
    
    /// When the class is being deinitialized, stop observing for Echo Objects
    deinit
    {
        self.stopObserving()
    }
}

/// The Echo Echoable base UITableViewController class. Any UITableViewController
/// that inherits from this will be able to observe Echo Objects
class EchoableTableViewController: UITableViewController, Echoable
{
    /// The default Echo Observer observer handler. If you do not override this in a subclassed view controller,
    /// nothing will happen
    func observe(object: EchoObject,
                 dispatcher: Dispatchable,
                 toObservers: [String])
    {
        // Handled by subclasses of EchoableTableViewController
    }
    
    /// When the class is being deinitialized, stop observing for Echo Objects
    deinit
    {
        self.stopObserving()
    }
}

/// The Echo Echoable base UINavigationController class. Any UINavigationController
/// that inherits from this will be able to observe Echo Objects
class EchoableNavigationController: UINavigationController, Echoable
{
    /// The default Echo Observer observer handler. If you do not override this in a subclassed view controller,
    /// nothing will happen
    func observe(object: EchoObject,
                 dispatcher: Dispatchable,
                 toObservers: [String])
    {
        // Handled by subclasses of EchoableNavigationController
    }
    
    /// When the class is being deinitialized, stop observing for Echo Objects
    deinit
    {
        self.stopObserving()
    }
}

