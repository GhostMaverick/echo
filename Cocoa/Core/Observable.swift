//
//  Observable.swift
//  Echo
//
//  Created by Kai Lamarr on 11/5/17.
//  Copyright © 2017 Kai Lamarr. All rights reserved.
//  __version__ 2.0 (12/15/2019)

import UIKit

/// Echo's Observable class Protocol
protocol Observable: class
{
    /// name (class): The name of the Observable class, usually just the name of the class itself
    static var name: String { get set }
    
    /// name (class): The name of the Observable class, usually just the name of the class itself
    var name: String { get set }
    
    /// Start Observing dispatched Echo Objects
    func startObserving()
    
    /// Stop Observing dispatched Echo Objects
    func stopObserving()
    
    /// Whether or not the Observable is currently observing for dispatched objects
    var isObserving: Bool { get }
    
    /// Observe a dispatched Echo Object
    /// - parameter notificationName: The name of the Notification containing the Echo object
    /// - parameter object: The Echo Object that was emitted
    /// - parameter dispatcher: The Echo Dispatcher that emitted the Echo Object
    /// - parameter toObservers: The intended Observer Echo names for the Echo Object.
    func observe(object: EchoObject,
                 dispatcher: Dispatchable,
                 toObservers: [String])
}

/// The Observable Protocol default extension implementation
extension Observable
{
    /// name (class): The name of the Observable class, usually just the name of the class itself
    static var name: String {
        get {
            let className = String(describing: type(of: self))
            return className.components(separatedBy: ".").first ?? ""
        }
        set { self.name = newValue }
    }
    
    /// name (class): The name of the Observable class, usually just the name of the class itself
    var name: String {
        get { return String(describing: type(of: self)) }
        set { self.name = newValue }
    }
    
    /// Whether or not the Observable is currently observing for dispatched objects
    var isObserving: Bool {
        get { return Echo.observers[self.name] != nil }
    }
    
    /// Start Observing dispatched Echo Objects
    func startObserving()
    {
        Echo.addObserver(self)
    }
    
    /// Stop Observing dispatched Echo Objects
    func stopObserving()
    {
        Echo.removeObserver(self)
    }
    
    /// An Observer -observe helper function that makes it easier and cleaner to handle observed objects.
    /// - parameter object: The emitted echo object that you want to use. Usually passed in by -observe.
    /// - parameter dispatcher: The Dispatchable that emitted the Echo Object. Usually passed in by -observe.
    /// - parameter toObservers: The applicable Observable names that you want to listen to. Usually passed in by -observe.
    /// - parameter target: The target EchoObject name that you want to listen for. Usually passed in by -observe.
    /// - parameter onObservation: The callback function called when a matching Observer name and EchoObject name is received.
    func observeFor(object: EchoObject,  dispatcher: Dispatchable, toObservers names: [String], target: String, onObservation: ((Any) -> Void)? = nil)
    {
        guard names.contains(self.name) else { return }
        
        Echo.log(message: "\(self.name) is handling the \(object) object with an observeFor \(target) Target.")
        
        if object.name == target {
            onObservation?(object.value)
        }
    }
}

/// The Echo Observable base UIViewController class. Any UIViewController
/// that inherits from this will be able to observe Echo Objects
class ObservableViewController: UIViewController, Observable
{
    /// The default Echo Observer observer handler. If you do not override this in a subclassed view controller,
    /// nothing will happen
    func observe(object: EchoObject,
                 dispatcher: Dispatchable,
                 toObservers: [String])
    {
        // Handled by subclasses of ObservableViewController
    }
    
    /// When the class is being deinitialized, stop observing for Echo Objects
    deinit
    {
        self.stopObserving()
    }
}

/// The Echo Observable base UITableViewController class. Any UITableViewController
/// that inherits from this will be able to observe Echo Objects
class ObservableTableViewController: UITableViewController, Observable
{
    /// The default Echo Observer observer handler. If you do not override this in a subclassed view controller,
    /// nothing will happen
    func observe(object: EchoObject,
                 dispatcher: Dispatchable,
                 toObservers: [String])
    {
        // Handled by subclasses of ObservableViewController
    }
    
    /// When the class is being deinitialized, stop observing for Echo Objects
    deinit
    {
        self.stopObserving()
    }
}

/// The Echo Observable base UINavigationViewController class. Any UINavigationViewController
/// that inherits from this will be able to observe Echo Objects
class ObservableNavigationViewController: UINavigationController, Observable
{
    /// The default Echo Observer observer handler. If you do not override this in a subclassed view controller,
    /// nothing will happen
    func observe(object: EchoObject,
                 dispatcher: Dispatchable,
                 toObservers: [String])
    {
        // Handled by subclasses of ObservableViewController
    }
    
    /// When the class is being deinitialized, stop observing for Echo Objects
    deinit
    {
        self.stopObserving()
    }
}
