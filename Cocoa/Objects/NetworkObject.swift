import Foundation

enum NetworkObject: EchoObject
{
    case networkIntervalUpdate(TimeInterval)
    case networkStatus(Network.Status)
    
    struct Target {
        static let networkIntervalUpdate = "networkIntervalUpdate"
        static let networkStatus = "networkStatus"
    }
    
    var name: String {
        switch self {
            case .networkIntervalUpdate:
                return Target.networkIntervalUpdate
            
            case .networkStatus:
                return Target.networkStatus
        }
    }
    
    var value: (Any) {
        switch self
        {
            case .networkIntervalUpdate(let newInterval):
                return (newInterval)
            
            case .networkStatus(let networkStatus):
                return (networkStatus)
        }
    }
}
