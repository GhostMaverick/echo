import Foundation

enum TimeObject: EchoObject
{
    case timePassed(Time.DateTime)
    case timeIntervalUpdated(TimeInterval)
    
    struct Target {
        static let timePassed = "timePassed"
        static let timeIntervalUpdated = "timeIntervalUpdated"
    }
    
    var name: String {
        switch self {
            case .timePassed:
                return Target.timePassed
            
            case .timeIntervalUpdated:
                return Target.timeIntervalUpdated
        }
    }
    
    var value: (Any) {
        switch self {
            case .timePassed(let dateTime):
                return(dateTime)
            
            case .timeIntervalUpdated:
                return ()
        }
    }
}
